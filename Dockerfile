FROM fedora
RUN apk add --no-cache python3 g++ make
WORKDIR /home/docker_image
COPY ..
CMD ["python3", "test.py"]
